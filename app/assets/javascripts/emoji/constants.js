export const FREQUENTLY_USED_KEY = 'frequently_used';
export const FREQUENTLY_USED_COOKIE_KEY = 'frequently_used_emojis';

export const CATEGORY_ICON_MAP = {
  [FREQUENTLY_USED_KEY]: 'history',
  people: 'smiley',
  nature: 'nature',
  food: 'food',
  activity: 'dumbbell',
  travel: 'car',
  objects: 'object',
  symbols: 'heart',
  flags: 'flag',
};

export const EMOJIS_PER_ROW = 9;
export const EMOJI_ROW_HEIGHT = 34;
export const CATEGORY_ROW_HEIGHT = 37;
